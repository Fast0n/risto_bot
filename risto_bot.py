import os
import sys
import telepot
from settings import token, start_msg, client_file
from time import sleep
from telepot.namedtuple import ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton
import json
import datetime
import matplotlib
from datetime import date, timedelta
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# State for user
user_state = {}


database = "database.csv"
a = {}
b = {}
c = {}
d = {}
e = {}
nTavolo = {}
markup_default = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[
    ["Apri tavolo", "Rapido"], ["Lista", "Statistiche"], ["Download"]])

markup_stats = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[
    ["Andamento giornaliero"], ["Andamento settimanale"], ["Scelta del prezzo fisso mensile"], ["Annulla"]])

markup_edit = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[
    ["Modifica orario", "Modifica prezzo fisso"], ["Annulla"]])

markup_time = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[["Adesso"], ["12:00", "12:15", "12:30", "12:45"], ["13:00", "13:15", "13:30", "13:45"], ["14:00", "14:15", "14:30", "14:45"], [
    "Annulla"], ["19:00", "19:15", "19:30", "19:45"], ["20:00", "20:15", "20:30", "20:45"], ["21:00", "21:15", "21:30", "21:45"], ["22:00", "22:15", "22:30", "22:45"]])

markup_fast = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[["Annulla"]])

def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    command_input = msg['text']

    # Check user state
    try:
        user_state[chat_id] = user_state[chat_id]
    except:
        user_state[chat_id] = 0

    # start command
    if command_input == "/start":
        if register_user(chat_id):
            bot.sendMessage(chat_id, start_msg, reply_markup=markup_default)

    if command_input.lower() == "annulla":
        bot.sendMessage(chat_id, "Operazione annullata",
                        reply_markup=markup_default)
        user_state[chat_id] = 0

    if command_input.lower() == 'rapido':
        user_state[chat_id] = 100

        if int(datetime.datetime.now().strftime('%M')) < 15:
            minutes = "00"
        elif int(datetime.datetime.now().strftime('%M')) < 30:
            minutes = "15"
        elif int(datetime.datetime.now().strftime('%M')) < 45:
            minutes = "30"
        elif int(datetime.datetime.now().strftime('%M')) <= 59:
            minutes = "45"

        a[chat_id] = datetime.datetime.now().strftime('%H') + ":" + minutes
        bot.sendMessage(
            chat_id, "Inserisci il Tavolo con il numero di persone es. ( 02@4 )", reply_markup=markup_fast)

    elif user_state[chat_id] == 100:
        with open(database, 'a') as the_file:
            the_file.write(datetime.datetime.now().strftime("%Y,%m,%d") + "," + a[chat_id] + "," + command_input.split("@")[0] + "," + command_input.split("@")[1] + "," +
                           "1" + "\n")

        bot.sendMessage(chat_id, "Tavolo aggiunto.",
                        reply_markup=markup_default)
        user_state[chat_id] = 0

    if command_input.lower() == 'apri tavolo':
        user_state[chat_id] = 1

        if int(datetime.datetime.now().strftime('%M')) < 15:
            minutes = "00"
        elif int(datetime.datetime.now().strftime('%M')) < 30:
            minutes = "15"
        elif int(datetime.datetime.now().strftime('%M')) < 45:
            minutes = "30"
        elif int(datetime.datetime.now().strftime('%M')) <= 59:
            minutes = "45"

        a[chat_id] = datetime.datetime.now().strftime('%H') + ":" + minutes

        markup = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[["Annulla"], ["01", "02", "03", "04", "05"], ["06", "07", "08", "09", "10"], [
            "11", "12", "13", "14", "15"], ["16", "18", "19", "20"], ["21", "22", "23", "24", "25"], ["26", "27", "30"]])
        bot.sendMessage(
            chat_id, "Inserisci il numero del tavolo...", reply_markup=markup)

    elif user_state[chat_id] == 1:
        b[chat_id] = command_input
        user_state[chat_id] = 3
        markup = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[["Annulla"], ["1", "2", "3", "4", "5"], ["6", "7", "8", "9", "10"], [
            "11", "12", "13", "14", "15"], ["16", "17", "18", "19", "20"], ["21", "22", "23", "24", "25"]])
        bot.sendMessage(
            chat_id, "Inserisci il numero delle persone...", reply_markup=markup)

    elif user_state[chat_id] == 3:
        c[chat_id] = command_input
        bot.sendMessage(chat_id, "Tavolo aggiunto.",
                        reply_markup=markup_default)
        user_state[chat_id] = 0

        with open(database, 'a') as the_file:
            the_file.write(datetime.datetime.now().strftime("%Y,%m,%d") + "," + a[chat_id] + "," + b[chat_id] + "," + c[chat_id] + "," +
                           "1" + "\n")

    if command_input.lower() == 'statistiche':
        user_state[chat_id] = 30
        bot.sendMessage(chat_id, "Scegli il tipo di stats",
                        reply_markup=markup_stats)

    elif user_state[chat_id] == 30:
        if command_input == "Andamento giornaliero":
            make_stats(command_input, datetime.datetime.now().strftime("%d"))
            bot.sendPhoto(chat_id, open('__pycache__/stats.png', 'rb'),
                          reply_markup=markup_default)
            user_state[chat_id] = 0

        elif command_input == "Andamento settimanale":
            days = []
            daysA = []

            tot_clienti = 0
            tot_incasso = 0
            tot_tavoli = 0

            for i in range(0, 7):
                now = datetime.datetime.now() - datetime.timedelta(days=i)
                if now.strftime("%A") != "Monday":
                    days.append(now.strftime("%Y,%m,%d"))
                    daysA.append(now.strftime("%A"))

            plt.figure(1, figsize=(25, 12.5))
            counter_tot = []
            counter_ayce_mattina = []
            counter_ayce_sera = []

            for i in range(0, 6):
                lines = open(database).read().split("\n")
                var = days[i]
                counter_12 = 0
                counter_13 = 0
                counter_14 = 0
                counter_19 = 0
                counter_20 = 0
                counter_21 = 0
                counter_22 = 0
                tot = 0
                ayce_mattina = 0
                ayce_sera = 0

                for j in range(0, len(lines)-1):
                    if lines[j].split(",")[0] == var.split(",")[0] and lines[j].split(",")[1] == var.split(",")[1] and lines[j].split(",")[2] == var.split(",")[2]:
                        tot += int(lines[j].split(",")[5])

                        if lines[j].split(",")[6] == "1" and int(lines[j].split(",")[3].split(":")[0]) in range(12, 15):
                            ayce_mattina += int(lines[j].split(",")[5]) * 12.90

                        if lines[j].split(",")[6] == "1" and int(lines[j].split(",")[3].split(":")[0]) in range(19, 23):
                            ayce_mattina += int(lines[j].split(",")[5]) * 21.90

                        if lines[j].split(",")[6] == "0" and int(lines[j].split(",")[3].split(":")[0]) in range(12, 15):
                            ayce_mattina += int(lines[j].split(",")[5]) * 10

                        if lines[j].split(",")[6] == "0" and int(lines[j].split(",")[3].split(":")[0]) in range(19, 23):
                            ayce_mattina += int(lines[j].split(",")[5]) * 15

                        if lines[j].split(",")[3].split(":")[0] == "12":
                            counter_12 = counter_12 + 1
                        if lines[j].split(",")[3].split(":")[0] == "13":
                            counter_13 = counter_13 + 1
                        if lines[j].split(",")[3].split(":")[0] == "14":
                            counter_14 = counter_14 + 1
                        if lines[j].split(",")[3].split(":")[0] == "19":
                            counter_19 = counter_19 + 1
                        if lines[j].split(",")[3].split(":")[0] == "20":
                            counter_20 = counter_20 + 1
                        if lines[j].split(",")[3].split(":")[0] == "21":
                            counter_21 = counter_21 + 1
                        if lines[j].split(",")[3].split(":")[0] == "22":
                            counter_22 = counter_22 + 1

                counter_tot.append(tot)
                counter_ayce_mattina.append(ayce_mattina)
                counter_ayce_sera.append(ayce_sera)

                if i == 0:
                    a1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

                if i == 1:
                    b1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

                if i == 2:
                    c1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

                if i == 3:
                    d1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

                if i == 4:
                    e1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

                if i == 5:
                    f1 = [counter_12, counter_13, counter_14,
                          0, counter_19, counter_20, counter_21, counter_22]

            values = [a1, b1, c1, d1, e1, f1]
            for i in range(1, 7):
                names = ['12-13', '13-14', '14-15', daysA[i-1],
                         '19-20', '20-21', '21-22', '22-23']

                plt.subplot(3, 2, i)
                plt.xticks(range(len(names)), names)
                plt.grid()
                plt.plot(values[i-1], color='#4285F4', linewidth=2.5)
                plt.plot(values[i-1], 'o', color='#0d5bdd')

                tot_clienti += counter_tot[i-1]
                tot_incasso += int(counter_ayce_mattina[i-1]) + \
                    int(counter_ayce_sera[i-1])
                tot_tavoli += sum(values[i-1])
                plt.suptitle('Statistiche settimanali ' + '(Clienti:' + str(tot_clienti) +
                             ' Tav:' + str(tot_tavoli) + ', Incasso:' + str(tot_incasso)+"€)")
                plt.legend(['(Clienti:' + str(counter_tot[i-1])+' Tav:' + str(sum(values[i-1]))

                            + ', Incasso:' +
                            str(int(counter_ayce_mattina[i-1]) +
                                int(counter_ayce_sera[i-1]))+"€)"
                            ])

            plt.savefig('__pycache__/stats.png')
            plt.close()

            bot.sendPhoto(chat_id, open('__pycache__/stats.png', 'rb'),
                          reply_markup=markup_default)
            user_state[chat_id] = 0

        elif command_input == "Scelta del prezzo fisso mensile":

            # get info from csv
            counter_yes = 0
            counter_no = 0
            lines = open(database).read().split("\n")
            for i in range(0, len(lines)-1):
                if lines[i].split(",")[6].split(":")[0] == "1":
                    counter_yes = counter_yes + 1
                if lines[i].split(",")[6].split(":")[0] == "0":
                    counter_no = counter_no + 1

            fig, ax = plt.subplots(
                figsize=(9, 6), subplot_kw=dict(aspect="equal"))

            recipe = [str(counter_yes) + " Prezzo_fisso",
                      str(counter_no) + " Prezzo_normale"]

            data = [float(x.split()[0]) for x in recipe]
            ingredients = [x.split()[-1].replace("_", " ") for x in recipe]

            def func(pct, allvals):
                absolute = int(pct/100.*np.sum(allvals))
                return "{:.1f}%({:d})".format(pct, absolute)

            wedges, texts, autotexts = ax.pie(data, autopct=lambda pct: func(pct, data),
                                              textprops=dict(color="w"))

            ax.legend(wedges, ingredients,
                      title="Legenda",
                      loc="center left",
                      bbox_to_anchor=(1, 0, 0.5, 1))

            plt.setp(autotexts, size=7, weight="bold")
            ax.set_title("Scelta del prezzo fisso mensile")

            plt.show()
            plt.savefig('__pycache__/stats.png')
            plt.close()
            bot.sendPhoto(chat_id, open('__pycache__/stats.png', 'rb'),
                          reply_markup=markup_default)
            user_state[chat_id] = 0

    if command_input.lower() == 'lista':
        lista = ""
        lines = open(database).read().split("\n")
        for i in range(0, len(lines)-1):
            if lines[i].split(",")[2] == datetime.datetime.now().strftime("%d") and int(datetime.datetime.now().strftime("%H"))-1 <= int(lines[i].split(",")[3].split(":")[0]):
                lista += lines[i].split(",")[4] + ","

        if len(lista) == 0:
            bot.sendMessage(
                chat_id, "Tutti i tavoli sono chiusi.")
        else:
            # sort list
            list1 = lista[:-1].split(',')
            for item in list1:
                item = int(item)

            list1.sort()
            lista = ' - '.join(list1)

            user_state[chat_id] = 50
            markup = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[
                lista.split(" - "), ["Annulla"]])
            bot.sendMessage(
                chat_id, "I tavoli aperti sono:\n"+lista, reply_markup=markup)

    elif user_state[chat_id] == 50:
        lista = ""
        lines = open(database).read().split("\n")
        for i in range(0, len(lines)-1):
            if lines[i].split(",")[4] == str(command_input) and lines[i].split(",")[2] == datetime.datetime.now().strftime("%d") and int(datetime.datetime.now().strftime("%H"))-1 <= int(lines[i].split(",")[3].split(":")[0]):
                lista += "N.Tav.: " + lines[i].split(",")[4] + "\nAperto alle: " +\
                    lines[i].split(",")[3] + "\nN.Persone: " + lines[i].split(",")[
                    5] + "\nPrezzo fisso: " + lines[i].split(",")[6].replace("0", "No").replace("1", "Si")
                nTavolo[chat_id] = lines[i]

        bot.sendMessage(chat_id, lista,
                        reply_markup=markup_edit)
        user_state[chat_id] = 60

    elif user_state[chat_id] == 60:
        if command_input == "Modifica orario":
            bot.sendMessage(chat_id, "Inserisci il nuovo orario:",
                            reply_markup=markup_time)
            user_state[chat_id] = 71

        elif command_input == "Modifica prezzo fisso":
            markup = ReplyKeyboardMarkup(
                resize_keyboard=True, keyboard=[["Si", "No"]])
            bot.sendMessage(chat_id, "Prezzo fisso?",
                            reply_markup=markup)
            user_state[chat_id] = 72

    elif user_state[chat_id] == 71:
        newHour = nTavolo[chat_id].split(",")[0] + "," + nTavolo[chat_id].split(",")[1] + "," + nTavolo[chat_id].split(",")[2] + "," + \
            command_input + "," + nTavolo[chat_id].split(",")[4] + "," + nTavolo[chat_id].split(",")[
            5] + "," + nTavolo[chat_id].split(",")[6]

        oldHour = nTavolo[chat_id].split(",")[0] + "," + nTavolo[chat_id].split(",")[1] + "," + nTavolo[chat_id].split(",")[2] + "," + \
            nTavolo[chat_id].split(",")[3] + "," + nTavolo[chat_id].split(",")[4] + "," + nTavolo[chat_id].split(",")[
            5] + "," + nTavolo[chat_id].split(",")[6]

        with open(database) as f:
            newText = f.read().replace(oldHour, newHour)

        with open(database, "w") as f:
            f.write(newText)

        bot.sendMessage(chat_id, "Operazione completata.",
                        reply_markup=markup_default)
        user_state[chat_id] = 0

    elif user_state[chat_id] == 72:
        ask = ""
        if command_input.lower() == "si":
            ask = "1"
        else:
            ask = "0"

        newHour = nTavolo[chat_id].split(",")[0] + "," + nTavolo[chat_id].split(",")[1] + "," + nTavolo[chat_id].split(",")[2] + "," + \
            nTavolo[chat_id].split(",")[3] + "," + nTavolo[chat_id].split(",")[4] + "," + nTavolo[chat_id].split(",")[
            5] + "," + ask

        oldHour = nTavolo[chat_id].split(",")[0] + "," + nTavolo[chat_id].split(",")[1] + "," + nTavolo[chat_id].split(",")[2] + "," + \
            nTavolo[chat_id].split(",")[3] + "," + nTavolo[chat_id].split(",")[4] + "," + nTavolo[chat_id].split(",")[
            5] + "," + nTavolo[chat_id].split(",")[6]

        with open(database) as f:
            newText = f.read().replace(oldHour, newHour)

        with open(database, "w") as f:
            f.write(newText)

        bot.sendMessage(chat_id, "Operazione completata.",
                        reply_markup=markup_default)
        user_state[chat_id] = 0

    if command_input.lower() == 'download':
        bot.sendDocument(chat_id, open(database, 'rb'))


def make_stats(name, function):
    # get info from csv
    counter_12 = 0
    counter_13 = 0
    counter_14 = 0
    counter_19 = 0
    counter_20 = 0
    counter_21 = 0
    counter_22 = 0
    counter_tot = 0
    counter_ayce_mattina = 0
    counter_ayce_sera = 0

    lines = open(database).read().split("\n")
    for i in range(0, len(lines)-1):
        if lines[i].split(",")[2] == function:
            counter_tot += int(lines[i].split(",")[5])

            if lines[i].split(",")[6] == "1" and int(lines[i].split(",")[3].split(":")[0]) in range(12, 15):
                counter_ayce_mattina += int(lines[i].split(",")[5]) * 12.90

            if lines[i].split(",")[6] == "1" and int(lines[i].split(",")[3].split(":")[0]) in range(19, 23):
                counter_ayce_mattina += int(lines[i].split(",")[5]) * 21.90

            if lines[i].split(",")[6] == "0" and int(lines[i].split(",")[3].split(":")[0]) in range(12, 15):
                counter_ayce_mattina += int(lines[i].split(",")[5]) * 10

            if lines[i].split(",")[6] == "0" and int(lines[i].split(",")[3].split(":")[0]) in range(19, 23):
                counter_ayce_mattina += int(lines[i].split(",")[5]) * 15

            if lines[i].split(",")[3].split(":")[0] == "12":
                counter_12 = counter_12 + 1
            if lines[i].split(",")[3].split(":")[0] == "13":
                counter_13 = counter_13 + 1
            if lines[i].split(",")[3].split(":")[0] == "14":
                counter_14 = counter_14 + 1

            if lines[i].split(",")[3].split(":")[0] == "19":
                counter_19 = counter_19 + 1
            if lines[i].split(",")[3].split(":")[0] == "20":
                counter_20 = counter_20 + 1
            if lines[i].split(",")[3].split(":")[0] == "21":
                counter_21 = counter_21 + 1
            if lines[i].split(",")[3].split(":")[0] == "22":
                counter_22 = counter_22 + 1
           # make a plot
    names = ['12-13', '13-14', '14-15', '---',
             '19-20', '20-21', '21-22', '22-23']
    values = [counter_12, counter_13, counter_14,
              0, counter_19, counter_20, counter_21, counter_22]
    # Set grid
    plt.grid()
    # Add plots
    plt.plot(values, color='#4285F4', linewidth=2.5)
    plt.plot(values, 'o', color='#0d5bdd')
    plt.xticks(range(len(names)), names)
    plt.suptitle(name+' (Clienti:' +
                 str(counter_tot) + ', Tav:' + str(counter_12 + counter_13 + counter_14 + counter_19 + counter_20 + counter_21 + counter_22)+', Incasso:' + str(int(counter_ayce_mattina) + int(counter_ayce_sera))+"€)")
    plt.savefig('__pycache__/stats.png')
    plt.close()


def register_user(chat_id):
    """
    Register given user to receive news
    """
    insert = 1

    try:
        f = open(client_file, "r+")

        for user in f.readlines():
            if user.replace('\n', '') == str(chat_id):
                insert = 0

    except IOError:
        f = open(client_file, "w")

    if insert:
        f.write(str(chat_id) + '\n')

    f.close()

    return insert


# Main
print("Avvio risto_bot")

# PID file
pid = str(os.getpid())
pidfile = "/tmp/risto_bot.pid"

# Check if PID exist
if os.path.isfile(pidfile):
    print("%s already exists, exiting!" % pidfile)
    sys.exit()

# Create PID file
f = open(pidfile, 'w')
f.write(pid)

# Start working
try:
    bot = telepot.Bot(token)
    bot.message_loop(on_chat_message)
    while(1):
        sleep(10)
finally:
    os.unlink(pidfile)
